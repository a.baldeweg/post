<?php

use PhpCsFixer\Finder;
use PhpCsFixer\Config;

if (!is_dir(__DIR__ . '/var/cache/')) {
    mkdir(__DIR__ . '/var/cache/', 0755, true);
}

$finder = Finder::create()
    ->in(__DIR__ . '/src')
    ->in(__DIR__ . '/tests')
;

$fileHeaderComment = <<<COMMENT
This script is part of baldeweg/post

Copyright 2018 André Baldeweg <kontakt@andrebaldeweg.de>
COMMENT;

return Config::create()
    ->setRules([
        '@PSR2' => true,
        'array_syntax' => [
            'syntax' => 'short'
        ],
        'header_comment' => [
            'header' => $fileHeaderComment,
            'separate' => 'both'
        ],
        'linebreak_after_opening_tag' => true,
        'ordered_imports' => [
            'sortAlgorithm' => 'alpha'
        ],
        'ordered_class_elements' => true,
        'phpdoc_order' => true
    ])
    ->setFinder($finder)
    ->setCacheFile(__DIR__ . '/var/cache/.php_cs.cache')
;

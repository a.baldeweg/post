# baldeweg/post

This package reads Markdown files containing YAML Front Matter from the filesystem and delivers you structured data back.

## Install

```shell
composer require baldeweg/post
```

## Usage

```php
use Baldeweg\Post\Post;

$post = new Post();
$file = $post->get('path/to/file');
```

Security: Please validate all paths passed to Post and also all data delivered!

## Dev

- bin/build - Reports and tests
- bin/lint - Checks for code standard violations and fixes them partially
- bin/phpunit - Runs the phpunit tests
- bin/tag VERSION - Sets a Git tag

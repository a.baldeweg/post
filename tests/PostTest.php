<?php

/*
 * This script is part of baldeweg/post
 *
 * Copyright 2018 André Baldeweg <kontakt@andrebaldeweg.de>
 */

namespace Baldeweg\Post\Tests;

use Baldeweg\Post\Post;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;

class PostTest extends TestCase
{
    public function testPost()
    {
        $file = <<<EOF
---
heading: test
description: description
---
# content
EOF;

        vfsStream::setup('root');
        file_put_contents(
            vfsStream::url('root/post.md'),
            $file
        );

        $post = new Post();

        $expected = [
            'meta' => [
                'heading' => 'test',
                'description' => 'description'
            ],
            'content' => "\n# content"
        ];

        $this->assertEquals(
            $expected,
            $post->get(
                vfsStream::url('root/post.md')
            )
        );
    }

    public function testEmptyPost()
    {
        $file = '';

        vfsStream::setup('root');
        file_put_contents(
            vfsStream::url('root/post.md'),
            $file
        );

        $post = new Post();

        $expected = [
            'meta' => null,
            'content' => ''
        ];

        $this->assertEquals(
            $expected,
            $post->get(
                vfsStream::url('root/post.md')
            )
        );
    }

    public function testException()
    {
        $this->expectException(\Exception::class);

        vfsStream::setup('root');

        $post = new Post();
        $post->get('test');
    }
}

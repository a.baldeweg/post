<?php

/*
 * This script is part of baldeweg/post
 *
 * Copyright 2018 André Baldeweg <kontakt@andrebaldeweg.de>
 */

namespace Baldeweg\Post;

interface PostInterface
{
    public function get(string $url): array;
}

<?php

/*
 * This script is part of baldeweg/post
 *
 * Copyright 2018 André Baldeweg <kontakt@andrebaldeweg.de>
 */

namespace Baldeweg\Post;

use Spatie\YamlFrontMatter\YamlFrontMatter;

class Post implements PostInterface
{
    public function get(string $url): array
    {
        if (!is_file($url)) {
            throw new \Exception('The requested file ' . htmlspecialchars($url) . ' does not exist!');
        }

        $file = file_get_contents($url) ?: null;
        if ($file === null) {
            return [
                'meta' => null,
                'content' => ''
            ];
        }
        $data = YamlFrontMatter::parse($file);

        return [
            'meta' => $data->matter(),
            'content' => $data->body()
        ];
    }
}
